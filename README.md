# Tsiminiya Maven Project Builder and Runner
This is a simple example of Docker Image which allows building and running of any Maven Project.

## Image Structure
### Volumes
| Location         | Description          |
|------------------|----------------------|
| /root/.m2        | Maven Root Folder    |
| /usr/javaproject | Maven Project Folder |

### Environment Variables
| Variable         | Description                                                                        |
|------------------|------------------------------------------------------------------------------------|
| APP_JAR_FILENAME | Resulting JAR file to be watched at the _target_ folder after _mvn clean install_. |

## How to use this image
### Docker CLI
By default, if you run the following command:
```bash
docker run -it tsiminiya/maven
```
a Maven Project _greetings_ will be built and run. Upon successful build and run, you'll see the last start-up logs like the ones below.
```
2018-08-22 08:41:29.096  INFO 110 --- [           main] o.s.b.a.w.s.WelcomePageHandlerMapping    : Adding welcome page: class path resource [static/index.html]
2018-08-22 08:41:29.258  INFO 110 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Registering beans for JMX exposure on startup
2018-08-22 08:41:29.322  INFO 110 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8888 (http) with context path ''
2018-08-22 08:41:29.330  INFO 110 --- [           main] c.rmitc.greetings.GreetingsApplication   : Started GreetingsApplication in 4.072 seconds (JVM running for 4.894)
```
#### Using the image into an actual Maven Project
If you want to use the image for your own Maven project (let's say your project is _boss-pokemon-weakness_), then you may build and run the project through the following Docker CLI command:

```bash
cd boss-pokemon-weakness
docker run -it -v ${PWD}:/usr/javaproject tsiminiya/maven
```
#### Custom Application JAR File Name
If you didn't specify in your _pom.xml_ to rename your resulting executable JAR as _app.jar_, then you need to specify the environment variable _APP_JAR_FILENAME_. So, you should modify the Docker CLI command a bit to:

```bash
cd boss-pokemon-weakness
docker run -it -v ${PWD}:/usr/javaproject -e APP_JAR_FILENAME="boss-pokemon-weakness-1.0-SNAPSHOT.jar" tsiminiya/maven
```
where the resulting application JAR is _boss-pokemon-weakness-1.0-SNAPSHOT.jar_.

### Docker Compose
The Docker Compose equivalent of the Docker CLI command above should be something like the following:

```yaml
version: 3
services:
  datetimeprinter:
    image: tsiminiya/maven
    volumes:
      - ${PWD}/boss-pokemon-weakness:/usr/javaproject
    environment:
      APP_JAR_FILENAME: boss-pokemon-weakness-1.0-SNAPSHOT.jar
```


For more Docker samples please visit http://www.rm-itc.com
