#1 Base Image: Tsiminiya JDK10 
FROM tsiminiya/jdk10

#2 Install Maven
RUN mkdir /usr/maven
COPY apache-maven-3.5.4-bin.tar.gz /usr/maven
RUN tar xvf /usr/maven/apache-maven-3.5.4-bin.tar.gz -C /usr/maven
ENV PATH=${PATH}:/usr/maven/apache-maven-3.5.4/bin

#3 Copy Maven Build Script
COPY scripts/build-and-run.sh /usr/javabuildscript

#4 Add Java Project to /usr/javaproject
ADD greetings /usr/javaproject

#5 Assign Working Directory
WORKDIR /usr/javaproject

#6 Create mountable directories or volumes
VOLUME [ "/root/.m2", "/usr/javaproject" ]

#7 Assign command to execute when container is run
CMD [ "sh", "/usr/mainscript/execute-scripts.sh" ]
