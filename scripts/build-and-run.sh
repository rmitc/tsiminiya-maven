#! /bin/sh

if [ -z "$APP_JAR_FILENAME" ]; then
    APP_JAR_FILENAME="app.jar"
fi

mvn clean install
cp /usr/javaproject/target/$APP_JAR_FILENAME /usr/javaapp
java -jar /usr/javaapp/$APP_JAR_FILENAME
